# Apache Spark cheat-sheet

> :warning: **DISCLAIMER** :warning:
>
> This document contains PSEUDO-CODE that is not supposed to run as it is.

## Create a Spark context

```python
# Create an executor for each core.
sc = pyspark.SparkContext('local[*]')

# Create _n_ executors.
sc = pyspark.SparkContext('local[n]')
```

Any IP address can be used instead of `local`. Only one spark context can exist.

## Say Hello World

```python
sc.parallelize(range(100)).count()
```

## Create an RDD

```python
# Create an RDD from a collection.
rdd = sc.parallelize(collection)

# Create an RDD from a file, using `/n` as line separator.
rdd = sc.textFile(file_path)
```

## Common transformations

```python
# Apply a function _f_ to each line. _f_ must be commutative.
rdd.map(lambda row: f(row))

# Keep only lines where the value of function _f_ is `True`.
rdd.filter(lambda row: f(row))

# Create more rows from one row. _f_ must return a `List` type.
rdd.flatMap(lambda row: f(row))

# Apply the reduce pattern _f_ to all rows with the same `key`. Works only for
# pair RDDs.
rdd.reduceByKey(lambda val1, val2: f(val1, val2))

# Sort rows according to _f_. It actually acts like an action, in the sense that
# it triggers the computation
rdd.sortBy(lambda row: f(row))

# Remove any duplicate element.
rdd.distinct()

# Join two RDDs. Both RDDs must be pair RDDs and their key must be serializable.
rdd.join(rdd2)

# Combine all the lines of two RDDs.
rdd.cartesian(rdd2)

# Merge two RDDs appending the first to the second.
rdd.union(rdd2)
```

## Common actions

Each action executes all transformations and then moves the result of the action
to the driver.

```python
# Move the first element to the driver.
rdd.first()

# Move first _n_ elements to the driver.
rdd.take(n)

# Move the biggest element to the driver, according to elements type.
rdd.max()

# Apply _f_ to each row, use the result to sort them and move the first _n_
# elements to the driver.
rdd.top(n, lambda row: f(row))

# Move all elements to the driver.
rdd.collect()

# Sum all elements and move the result to the driver.
rdd.sum()

# When applied to integers, move a tuple of useful statistics to the driver.
rdd.stats()
```

## Common pipelines

- `T:map` | `T:repartition` | `T:persist` | `A:count`

  Ensure a correct distribution of lines per worker:

  ```python
  # Associate a key that we know is well distributed - e.g. a random int between
  # 0 and a multiple of the number of workers.
  rdd.map(lambda row: f(row)) \
  # Force a re-shuffle to distribute the rows of rdd in _n_ partitions.
  .repartition(n) \
  # Force executors to persist that specific DAG to speed up re-use.
  .persist() \
  # Check if any line raise and exception.
  .count()
  ```

- `T:textFile` | `T:map` | `T:filter`

  ```python
  # Read a file
  rdd.textFile(filePath) \
  # Process the rows in some way
  .map(lambda row: f(row)) \
  # Choose which rows to keep
  .filter(lambda row: f(row))
  ```

## Patterns

```python
# Count a distribution.
rdd.map(lambda (key,val): (key, 1)).reduceByKey(lambda x, y: x + y)

# Get next, closest in value element for each element.
rdd.join(rdd) \
# Dictate the direction of the comparison, greatly reducing the amount of
# computation needed. `<=` is used instead of `<=` because otherwise the last
# value for each key would be have no next element.
.filter(lambda key, (val1, val2): val1 <= val2) \
# Make left value part of the key.
.map(lambda key, (val1, val2): ((key, val1), val2)) \
# Add delta between the two values.
.map(lambda (key, val1), val2: ((key, val1), val2, val2 - val1)) \
# Reduce to the smallest, non 0 delta.
.reduceByKey(lambda x, y: x if y == 0 else y if x == 0 else x if x < y else y)

# Improve distribution across _n_ workers when |key| << n.
# Include a random integer _m_ as part of the key.
rdd.map(lambda key, val: ((randInt(0,n), key), val)) \
# Reduce using (m, key) as key, to better distribute the computation.
.reduceByKey(lambda val1, val2: f(val1, val2)) \
# Restore previous structure of the elements.
.map(lambda (n, key), val: (key, val)) \
# Finish reducing, this time with a much smaller set of elements.
.reduceByKey(lambda val1, val2: f(val1, val2))

# Inverse document frequency: considering all words that appear in _n_ books,
# in how many books does each word appear? Rarest words are more important in
# computing similarity.
# Each row of the rdd has `key: book` and `val: List[Tuple[word, frequency]]`
# Flatten the list of words of each book to obtain a unique list.
idf = rdd.flatMap() \
# Consider each word just once for every book in which it appears.
.map(lambda book, (word, frequency): (book, (word, 1))) \
# Discard the bookId, since we are not interested in distinguishing anymore.
.map(lambda book, (word, _): (word, 1)) \
# Sum all the occurrences of the different words.
.reduceByKey(lambda n, m: n + m) \
.persist()

# From this result, we can compute the tdf_idf, that compares each word
# frequency with its rarity.
# Helper function to compute tdf_idf.
def tdf_idf(tdf, idf):
  return tdf * 1/idf
# Join the original rdd with the computed idf, in order to associate each
# (book, word) pair with the idf of the word.
rdd.join(idf) \
# Associate the tdf_idf to each (book, word) pair.
.map(lambda book, word, tdf, idf: (book, word, f(tdf, idf)))

# Join of registries using a dictionary. Works if the dictionary is broadcasted.
dictionary = rdd2.collectAsMap()
rdd1.map(lambda key, val: dictionary.get(key), val)
```

## Broadcast a variable

```python
broadcastedVar = sc.broadcast(var)
assert(var == broadcastedVar.value)
```
