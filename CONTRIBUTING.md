# Contributing

## Commit format

Be a good [commitizen](https://commitizen-tools.github.io/commitizen/)

## `pre-commit` hooks

Pre-commit hooks are available via [pre-commit](https://pre-commit.com/). Most -
if not all - of the checks done at this stage will be also performed by CI/CD.
To install them, run:

```bash
pre-commit install
pre-commit install --hook-type commit-msg
```
