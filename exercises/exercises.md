# Exercises

## Lab 1

### 1. McStatus

The McStatus dataset is obtained sampling the status of each machine at 1HZ. If
the status is the same as before, no lines are added. If not, a new line with
the specific Timestamp and the new StatusID is shown. First part of the
exercise, play with data:

- Create the Status RDD.
- Map each line to have a dictionary like

  ```json
  {
    "McID": "Machine_4",
    "TimeStamp": "2018-03-11T16:13:51.180Z",
    "StatusID": 3
  }
  ```

- Show the first line.
- When does the last event happen? And the first?
- Keep only data from 15 of January 2018.
- How many events per machine are inside this dataset?
- Compute the duration of each event as the difference between the actual event
  and the one right after.

### 2. StatusName

The McStatus dataset can be completed using the StatusName Registry. Pay
attention, it has been obtained using the CDC approach over the StatusName
table. First part of the exercise, play with data:

- Create the StatusName RDD.
- Map each line to have a dictionary like.

  ```json
  {
    "StatusID": 14,
    "StatusName": "14-MissingSupplies",
    "StatusType": 1,
    "Timestamp": "2019-01-14T22:58:31.579Z"
  }
  ```

- Show the first line.
- Show the "actual" version of the table, so take the correct value for each
  row.

### 3. Join

Combination of the log and the registry. First part of the exercise, play with
data:

- Join the two rdd using all the approaches you know.
  - How many lines do we have? Why?
  - Which approach performs better?
- Try to compute a more reasonable join.
- Compute for each status name | machine, the amount of time spent in each
  state.

## Lab 2

### 1. MSEG table

Find information here: <https://www.leanx.eu/en/sap/table/mseg.html>. MSEG
represent the warehouse status. To let you understand better the idea of CDC,
the full extraction from four different days are provided in four different
files.

First part of the exercise, play with data:

- Create a function to read the dataset of a specific day.
- Use aforementioned function to read the status of 2021-11-21.
- Create the Status RDD.
- Map each line to have a dictionary.
- Show the first line.
- Build a cdc logic in Apache Spark to write as output only fresh lines. Feel
  free to write on top any additional file to keep the status.
  - Simulate it is 2021-11-21 read and write
  - Simulate now is 2021-11-22:
    - Write the fresh lines w.r.t. the previous execution

### 2. MSEG Analytics

1. Compute the snapshot (i.e., the same version of the table that is inside the
   Database)
2. Using the CDC version, compute for each MATNR
   1. The average quantity (MENGE) the amount of time the quantity changed
   2. the top 10 MATNR by std deviation of changes
   3. the similarity between each component as the similarity of the MENGE
      vectors
   4. the most "correlated" couple of items

## Exam 20-01-2022

### Dataset

Transactions

| Field         | Type     |
| ------------- | -------- |
| TransactionID | Int      |
| Timestamp     | Datetime |
| UserID        | String   |
| CompanySymbol | String   |
| Volume        | Int      |
| Action        | String   |

Prices

| Field         | Type     |
| ------------- | -------- |
| CompanySymbol | String   |
| Timestamp     | Datetime |
| ValuePerUnit  | Float    |

CompanyInfo

| Field         | Type   |
| ------------- | ------ |
| CompanySymbol | String |
| CompanyName   | String |
| Address       | String |

### Q1

Define the type of each table - Log or Registry - and architect a strategy to
integrate them through a CDC job.

### Q2

Compute:

1. The total number of transactions.
2. The number of transactions done by the user `HAL9000`.
3. The number of transactions per day.
4. The average daily transactions per company during the week 42 of 2021.
5. The total amount of Euro spent by each user.
6. The value of the portfolio of each user.

## Production Monitoring

The plant director would like to control and master the production. The plant
works on 20 shifts per week, 8 hour per shift with 3 breaks: 2 15 minutes break
after 2 hours of work and the 45 minutes lunch break. The line works with a
tense flow (i.e., no one can change the order of operations or force a rework:
the only operation permitted is to scrap a piece before the end). As of today
three different systems are in place:

1. MES - Manufacturing Execution System: Track the output of each machine, as
   well as the status of the machine itself. It contains following tables:

   1. Cycle Time Table - the amount of time spent to create a piece

      | Field      | Type     |
      | ---------- | -------- |
      | Timestamp  | Datetime |
      | ItemID     | String   |
      | ItemTypeID | String   |
      | Cycle Time | Int      |
      | Status     | String   |

      Status=”OK”|”KO”, ItemTypeID is the link with BOM FinishGoodID

   2. Machine Status Table - keep trace when a machine change state

      | Field     | Type     |
      | --------- | -------- |
      | Timestamp | Datetime |
      | MachineID | String   |
      | Status    | String   |

      Status=”Waiting for Component”|”Waiting”|”Working”|”Error”|”Stop”|”Break”

2. ERP: Track financial flows inside the company, as raw material received - raw
   material consumed - finish good created - finish good spent. It contains
   following tables:

   1. Material Movement - the logical/financial movement of a given material
      from a status to the other

      | Field     | Type     |
      | --------- | -------- |
      | ItemID    | String   |
      | Timestamp | Datetime |
      | Position  | String   |

      Position=”material received from supplier” - “material consumed” -
      “material created” - “material sent to customer”

   2. Bill of Material - the composition of raw material per each finish good

      | Field         | Type   |
      | ------------- | ------ |
      | FinishGoodID  | String |
      | RawMaterialID | String |
      | Quantity      | Int    |

   3. Raw Material Registry - price could change over time

      | Field         | Type     |
      | ------------- | -------- |
      | RawMaterialID | String   |
      | Price         | Int      |
      | Timestamp     | Datetime |

From a logical perspective, the sequence of operation to produce an item is:

- Needed Raw Materials are received and stocked inside the internal Warehouse
- Inside the ERP, there is the exact recipe to create the given item
- The production - station by station -consumes physically needed raw material
  (without notifying to the internal Warehouse) for that given transformation to
  create the finish good
- At the end of the line, if the status of the last operation - as well as of
  all the other before it - is “OK”, the Finished Good is created and an
  operation of “Backflush” is called to logically consume all the material
  inside the BOM in the Warehouse.

Data is stored inside HDFS with the following structure, using a CDC pattern:

- Root
  - [DATA] ERP - Bill of Material
  - Plant Name
    - [DATA] ERP - Material Movement
    - Line Name
      - [DATA] MES - Machine Status Table
      - Station Name (or MachineID)
        - [DATA] MES - Cycle Time Table

Pay Attention! Some information must be implied by the exact position of the
file.

### Questions

1. What is the nature of each table (log|registry)?
2. Describe for each table how a CDC job could be created to move the data on
   HDFS
3. Design an Enterprise Architecture to manage this system, estimating cost (and
   possible benefits)

### Spark

1. Compute the average number of pieces per day and per line
2. Compute the average machine time for each station (i.e., machine time is the
   time between the conclusion of a piece and the conclusion of the piece right
   after)
   1. What is the impact of the break to the machine time?
   2. How can we mitigate it?
3. Show the distribution of machine status per day
4. Count the amount of pieces done per day in any given status (a piece is
   considered done in a given status if it is completed before the machine
   change status)
5. Show the correlation between the amount of pieces done and the amount of time
   spent on “Waiting for component” status
6. Build the line transition matrix for each line to get the line topology
   (which is the order of stations inside the line)
   1. Check the “tense flow line” assumption is always true
   2. Count the amount of scraps as the amount of pieces which didn’t finish the
      full line flow + the amount of “KO” pieces at the end of the line
7. For each scrap (obtained on 6.b or, if you have skipped that step, consider
   only the “KO” status of Cycle Time Table), compute the amount of raw material
   scrapped
8. Compute the number of “Backflush errors” as the difference between End of
   Line “OK” pieces against the one declared in ERP
9. Compute the total cost of 1 month of production for each item
   1. Using only the last value of Material Registry
   2. Using the average value of Material Registry
   3. Using the correct value of Material Registry per each piece

## Churn out prediction

A Video On Demand Provider called myVideo would like to know its audience
better. myVideo stores only start play events on a transactional Database. There
we can find following tables:

Event Table

| Field     | Type     |
| --------- | -------- |
| Timestamp | Datetime |
| UserID    | String   |
| ItemID    | String   |

User Table

| Field            | Type     |
| ---------------- | -------- |
| UserID           | String   |
| RegistrationDate | Datetime |
| BirthDate        | Datetime |
| SubscriptionTag  | String   |

SubscriptionTag=”Active”|”Churning Out”|”Churned Out”

Video Table

| Field         | Type   |
| ------------- | ------ |
| ItemID        | String |
| Genre         | String |
| LengthSeconds | Int    |

Series Table

| Field      | Type   |
| ---------- | ------ |
| ItemID     | String |
| SeriesID   | String |
| SeriesName | String |

### Questions

1. What is the nature of each table (log|registry)?
2. Design a CDC job to write deltas of each table on a HDFS location.

### Spark

1. Compute the average items watched per day per user
2. Compute the average watch duration per user (for all of them?)
3. Design a monthly job to detect churned out users
4. Design a weekly job to detect going-to-churn-out users

## Exam 2022-02-07

ERP is an acronym that stands for enterprise resource planning (ERP). It's a
business process management software that manages and integrates a company's
financial, supply chain, operations, commerce, reporting, manufacturing, and
human resource activities. One important ERP entity is the Invoice: it is the
only way for a business to business company to get money from customer or to pay
suppliers. Each invoice is made by several part like the header – the part with
general information about customers/suppliers that define the invoice – the list
of items, the list of payments, details about the customers, details about the
shipping. The exam dataset focuses only on the financial part, so header and
payments:

Invoice Header

Header of Invoice fiscal documents of our company, with all the information: it
is created in the moment an invoice is issued. For legal reason, is not possible
to change an invoice right after: if there is the need to change it, it should
be put in “Cancelled” status and then created one new from scratch! At the
beginning of each fiscal year, the DocumentNumber start back from “00000000”

- 2 Signs: “+” – if the money must be paid from the receiver – or “-“ – if the
  money must be paid from our company
- CustomerId could be both a Customer if the invoice is from Account Receivable
  or a Supplier if the invoice is from Account Payable
- PostingDate: the date the invoice is issued (i.e., created)
- DueDate is the day the last day the customer can pay us as per the contract
  done with the customer

| Field          | Type      |
| -------------- | --------- |
| DocumentNumber | Int       |
| FiscalYear     | Int       |
| PostingDate    | Datetime  |
| CustomerId     | String    |
| GrossValue     | Float     |
| Sign           | String    |
| DueDate        | Timestamp |
| DocumentType   | String    |

DocumentTpe: [“Invoice”,”Debit Note”, “Credit Note”, “Cancelled”]

Invoice Payments

Set of payment given or taken for a given Invoice Header. Each invoice could be
paid with multiple payments: every time a new payment is done, a “Closed”
payment line is added with the amount of money paid as well as an “Open” line is
eventually created with the amount of money still due

- PaymentStatus: Open, if the customer has not already paid us – it represents
  the actual amount of money still the customer should give us; Closed, if
  amount of money already paid

| Field            | Type     |
| ---------------- | -------- |
| DocumentNumber   | Int      |
| FiscalYear       | Int      |
| PaymentTimestamp | Datetime |
| PaymentStatus    | String   |
| GrossValue       | Float    |
| Sign             | String   |

### Q1

- Define the type of each table (Log or Registry): which are the keys of these
  tables?
- Architect a strategy to integrate them through a CDC job. You can propose a
  pseudo-code solution for the CDC job, a SQL based solution, a mix of the two.
- Architect a spark based job that reads the cdc files created after one month
  of CDC daily jobs to create an “InvoiceStatusObject”: imagine it is the first
  time it runs so there is no need to add any incremental logic, just read all
  the files and compute the logic and write the result. Imagine previous step
  write a file with the timestamp in the file name in the same folder, and you
  can use wildcard expression to read multiple files together.
- Architect a last job that apply the cdc to get changes over the
  “InvoiceStatusObject”.

### Q2

Assumption: you have already read the 2 tables and you have a session with
InvoiceHeaderRDD and InvoicePaymentsRDD where you find read any cdc created over
time. Write the code to compute:

- How many customers has my company?
- Extract the list of “Still Open” invoices with this structure

  | Field                | Type                                         |
  | -------------------- | -------------------------------------------- |
  | DocumentNumber       | Int                                          |
  | FiscalYear           | Int                                          |
  | PostingDate          | Datetime                                     |
  | CustomerId           | String                                       |
  | GrossValue           | Float                                        |
  | Sign                 | String                                       |
  | DueDate              | timestamp                                    |
  | listOfClosedPayments | list<{“PaymentDate”:Datetime,”Value”:Float}> |
  | OpenPayment          | dict<{“PaymentDate”:Datetime,”Value”:Float}> |

- Total gross value of “Still Open” invoices posted during 2021 that should be
  still paid.
- Create a score of “reliability”:
  1. By Document. The score for a document is based on the amount of day after
     posting an invoice needed to pay it: each day after the due date must be
     count as +5 (e.g., if an invoice is posted on 01/01 with a due date of
     01/20 and paid on 01/15, the score for this invoice is +15. The same
     invoice paid on 01/25 generate a score of +45). This score must be
     multiplied by the GrossAmount of that given invoice.
  2. The monthly score for each customer is the average score of each invoice in
     that month.
  3. The global score for each customer is the median score of each month.

## Challenge 1

Compute average and median of a dataset.

## Challenge 2

Given 5 documents, identify the most similar pair among them, using cosine
similarity as metric.
