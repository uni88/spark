# This code belongs to brcondor. Check out the original code on his repository:
# https://github.com/brcondor/Architectures_For_Big_Data

from abc import ABC, abstractmethod
import json
from datetime import datetime, timedelta
import sys
