import hashlib
from dataclasses import dataclass, field
from dataGenerator.datasetGenerator import typeGenerator, datasetGenerator


@dataclass
class Column:
    generator: typeGenerator
    keyName: str
    args: dict = field(default_factory=dict)


def asDict(row, keyList):
    return {keyList[n]: row[n] for n in range(len(keyList))}


def readCsv(sparkContext, year, month, day, sep, header):
    return (
        sparkContext.textFile(f"./data/Lab002_{year}{month}{day}.csv")
        .filter(lambda row: row != header)
        .map(lambda row: row.split(sep))
    )


def hashValues(*args):
    return hashlib.md5(repr(tuple(args)).encode("utf-8")).hexdigest()


def hashRow(row, rowKeys, rowValues):
    return {
        "khash": hashValues([row.get(key) for key in rowKeys]),
        "hash": hashValues([row.get(value) for value in rowValues]),
    }


def generate(columns, rows, type="log"):
    generator = datasetGenerator(type)
    for column in columns:
        generator.addGenerator(column.generator, column.keyName, **column.args)
    return generator.generateDataset(rows)
